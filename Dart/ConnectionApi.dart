import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart';
import 'package:intl/intl.dart';

void main() {
    String publicKey = "YOUR-PUBLIC-KEY";
    String privateKey = "YOUR-PRIVATE-KEY";
    String method = "GET";
    String path = "/user";
    var f = DateFormat('EEE, dd MMM yyyy HH:mm:ss');
    var date = "${f.format(DateTime.now().toUtc())} GMT";
    String contentToSign = '$method$path$date$publicKey';
    var key = utf8.encode(privateKey);
    var bytesG = utf8.encode(contentToSign);
    var hmacSha256 = Hmac(sha256, key);
    var digest = hmacSha256.convert(bytesG);
    var signature = digest;
 
    String authorizationString = "hmac $publicKey:$signature";
    fetchAlbum(authorizationString, date);
}

Future<http.Response> fetchAlbum(authorizationString, date) async {
  String collection;
  Map<String, String> headers = {
    "Content-type": "application/json",
    "Accept": "application/json",
    "Authorization": authorizationString,
    "Request-Date": date
    };
  // CONNECT WITH USER ENDPOINT
  var request = await http.Request('GET',Uri.parse('https://api.fieldclimate.com/v2/user'));
  request.headers.addAll(headers);
 
  http.StreamedResponse streamedResponse = await request.send();
  var response = await http.Response.fromStream(streamedResponse);
  print(response.body);
}
