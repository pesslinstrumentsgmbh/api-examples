<?php

header("Access-Control-Allow-Origin", "*");
header("Content-type", "application/json");

$content = file_get_contents("stationsRequest.php");
$stations = json_decode($content, true);   //array

$data = [];

foreach ($stations as $station)
{
   // if(isset($station['name']['custom'])){
        $output = [
            "name" => $station['name']['original'],
            "customName" => $station['name']['custom'],
            "pos" => [
                "lng" => $station['position']['geo']['coordinates'][0],
                "lat" => $station['position']['geo']['coordinates'][1],
            ],
            "rain" => [
                "24h" => $station["meta"]["rain24h"]["sum"],
                "48h" => $station["meta"]["rain48h"]["sum"],
                "7d"  => $station["meta"]["rain7d"]["sum"],
            ]
        ];

     //we ignore coordinates (0,0)
     if($output['pos']['lng'] != 0 && $output['pos']['lat'] != 0)
        $data[] = $output;
}

echo json_encode($data);

?>
