<?php

/**
 * How to generate your HMAC keys
 * https://support.metos.at/en/support/solutions/articles/15000018359-create-hmac-api-keys-using-fieldclimate
 **/

$API_URL =  "https://api.fieldclimate.com/v2";
$public_key = "PUBLIC_KEY";
$private_key = "PRIVATE_KEY";
$method = "POST";
/**
 * Your Station ID with 8 char
 */
$stationID = "0000000";

/**
 * The documentation for the endpoint forecast:
 * https://docsdev.fieldclimate.com/#25e0e127-e8e8-4b04-989d-8889ad8fe7eb
 */
$request = "/forecast/{$stationID}";

/**
 * HMAC authentication needs
 */
$timestamp = gmdate('D, d M Y H:i:s T');
$content_to_sign = $method . $request . $timestamp . $public_key;
$signature = hash_hmac("sha256", $content_to_sign, $private_key);
$headers = [
    "Accept: application/json",
    "Authorization: hmac {$public_key}:{$signature}",
    "Date: {$timestamp}"
];

/**
 * Forecast 3 days: general3
 * Forecast 7 days: general7
 */
$payload = ["name" => "general3"];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $API_URL . $request);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
if ($method == 'POST') {
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload, true));
}
$output = curl_exec($ch);
curl_close($ch);

header("Content-type", "application/json");
echo $output . PHP_EOL;
