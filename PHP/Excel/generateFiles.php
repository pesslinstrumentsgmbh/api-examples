<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

header("Access-Control-Allow-Origin", "*");
header("Content-type", "application/json");

$stations = file_get_contents("getStationData.php");
$stations = json_decode($stations, true);             //array
$stationsArray = array_chunk($stations, 10);

$styleAll = [
    'font' => [
        'name' => 'Calibri',
        'size' => '12'
    ],
    'alignment' => [
        'horizontal' => 'center',
        'vertical' => 'center'
    ],
];
$styleHeaders = [
    'font' => [
        'size' => '14',
        'bold' => true
    ]
];
$styleBorderOutlineMedium = [
    'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
        ],
    ],
];
$styleBorderRightThin = [
    'borders' => [
        'right' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ],
];
$styleBorderRightMedium = [
    'borders' => [
        'right' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
        ],
    ],
];
$styleBorderBottomThin = [
    'borders' => [
        'bottom' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ],
];

$spreadsheet = new Spreadsheet();
$spreadsheet->getDefaultStyle()->applyFromArray($styleAll);
$spreadsheet->removeSheetByIndex(0);

$file = 0;
foreach($stationsArray as $group)
{
    $file++;
    $spreadsheet = new Spreadsheet();
    $spreadsheet->getDefaultStyle()->applyFromArray($styleAll);
    $spreadsheet->removeSheetByIndex(0);
    foreach ($group as $station)
    {
        //create sheet
        $sheet = $spreadsheet->createSheet();
        $sheet->setTitle($station['stationID']);

        $dates = $station['dates'];
        $allData = $station['data'];
        $column = 1;
        $columnString = "A";
        $columnData = 2;
        $columnDataString = "B";

        //STATION ID
        $sheet->setCellValueByColumnAndRow($column, 1, $station['stationID']);
        $sheet->getStyle("A1")->applyFromArray($styleHeaders);

        //DATE / TIME (column = 1)
        $sheet->setCellValueByColumnAndRow($column, 2, "DATE / TIME");
        $sheet->getStyle("A2")->applyFromArray($styleHeaders);

        $columnDates = array_chunk($dates, 1);
        $sheet->fromArray(
            $columnDates,           //The data to set
            null,
            'A3',           //Top left coordinate
            true   //show null values
        );
        $sheet->getStyle("A1:" . $columnString . $sheet->getHighestRow())->applyFromArray($styleBorderRightMedium);

        //HEADERS
        foreach ($allData as $oneData) {
            $cell = "";
            $mergeCell = "";
            $column++;
            $columnString++;

            $keys = array_keys($oneData['values']);

            $cell .= $columnString . "1";

            $sheet->setCellValueByColumnAndRow($column, 1, strtoupper($oneData['name']));
            $sheet->getStyle($cell)->applyFromArray($styleHeaders);

            //MERGE CELLS if necessary, borders
            for ($i = 1; $i < count($keys); $i++) {
                $column++;
                $columnString++;
            }
            $mergeCell = $columnString . "1";
            $sheet->mergeCells("$cell:$mergeCell");
            $sheet->getStyle($mergeCell . ":" . $columnString . $sheet->getHighestRow())->applyFromArray($styleBorderRightThin);

            foreach ($keys as $key) {
                $sheet->setCellValueByColumnAndRow($columnData, 2, strtoupper($key));

                $data = array_chunk($oneData['values'][$key], 1);       //column
                $startCell = $columnDataString . "3";

                $sheet->fromArray(
                    $data,                 //The data to set
                    null,
                    $startCell,            //Top left coordinate
                    true  //show null values
                );

                $columnData++;
                $columnDataString++;
            }
        }

        // COLUMNS and ROWS
        $lastColumn = $sheet->getHighestColumn();
        $lastRow = $sheet->getHighestRow();
        foreach (range('B', $lastColumn) as $column) {
            $sheet->getColumnDimension($column)->setWidth(21);
        }
        foreach (range('1', '2') as $row) {
            $sheet->getRowDimension($row)->setRowHeight(25);
        }
        $sheet->getColumnDimension("A")->setAutoSize(true);

        // BORDERS
        $sheet->getStyle('A1:' . $lastColumn . '2')->applyFromArray($styleBorderOutlineMedium);
        $sheet->getStyle('A1:' . $lastColumn . $lastRow)->applyFromArray($styleBorderOutlineMedium);

        $sheet->getStyle("A1:" . $lastColumn . "1")->applyFromArray($styleBorderBottomThin);

        $writer = new Xlsx($spreadsheet);
        $writer->save("Stations" . $file. ".xlsx");
    }
}

?>