<?php

// Clients public and private key provided by service provider
$public_key = "PUBLIC_KEY";
$private_key = "PRIVATE_KEY";

$userStations = file_get_contents("getUserStations.php");
$userStations = json_decode($userStations, true);        //array
$language = preg_replace("/[^a-zA-Z]/", "", file_get_contents("getUserData.php"));

// Define the request parameter's
$method = "GET";

$data = [];
$st = [];

foreach($userStations as $stationID)
{
    $request = "/data/" .$stationID. "/raw/last/24h";

    $timestamp = gmdate('D, d M Y H:i:s T'); // Date as per RFC2616 - Wed, 25 Nov 2014 12:45:26 GMT

    // Creating content to sign with private key
    $content_to_sign = $method.$request.$timestamp.$public_key;

    // Hash content to sign into HMAC signature
    $signature = hash_hmac("sha256", $content_to_sign, $private_key);

    // Add required headers
    // Authorization: hmac public_key:signature
    // Date: Wed, 25 Nov 2014 12:45:26 GMT
    $headers = [
        "Accept: application/json",
        "Accept-language: ".$language,
        "Authorization: hmac {$public_key}:{$signature}",
        "Date: {$timestamp}"
    ];
    // Prepare and make https request
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.fieldclimate.com/v2" . $request);

    // SSL important
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $output = curl_exec($ch);
    curl_close($ch);
   // $output = json_decode($output, true);

    $station = json_decode($output, true);

    $st = [
        "stationID" => $stationID,
        "dates" => $station['dates'],
    ];

    for($i = 0; $i < count($station['data']); $i++)
    {
        $st['data'][$i]['name'] = $station['data'][$i]['name'];
        $st['data'][$i]['values'] = $station['data'][$i]['values'];
    }

    $data[] = $st;

    // Parse response as json and work on it ..
    header("Content-type", "application/json");
}

echo json_encode($data);






