<?php

/**
 * How to generate your HMAC keys
 * https://support.metos.at/en/support/solutions/articles/15000018359-create-hmac-api-keys-using-fieldclimate
 **/

$public_key = "PUBLIC_KEY";
$private_key = "PRIVATE_KEY";
$method = "GET";
/**
 * Getting user stations
 **/
$request = "/user/stations";

/**
 * HMAC authentication needs
 */
$timestamp = gmdate('D, d M Y H:i:s T');
$content_to_sign = $method . $request . $timestamp . $public_key;
$signature = hash_hmac("sha256", $content_to_sign, $private_key);
$headers = [
    "Accept: application/json",
    "Authorization: hmac {$public_key}:{$signature}",
    "Date: {$timestamp}"
];

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.fieldclimate.com/v2" . $request);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
if ($method == 'POST') {
    curl_setopt($ch, CURLOPT_POST, 1);
}
$output = curl_exec($ch);
curl_close($ch);

header("Content-type", "application/json");
echo $output . PHP_EOL;
