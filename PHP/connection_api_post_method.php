<?php

/**
 * How to generate your HMAC keys
 * https://support.metos.at/en/support/solutions/articles/15000018359-create-hmac-api-keys-using-fieldclimate
 **/

$API_URL =  "https://api.fieldclimate.com/v2";
$public_key = "PUBLIC_KEY";
$private_key = "PRIVATE_KEY";
$method = "POST";
/**
 * Your Station ID with 8 char
 */
$stationID = "00000000";
/**
 * Data aggregation could be;
 * - raw,
 * - hourly,
 * - daily,
 * - monthly,
 */
$dataGroup = "hourly";
/**
 * Unix timestamp format
 */
$from = 1663602301;
/**
 * Unix timestamp format
 */
$to = 1663775101;
/**
 * The documentation for the endpoint data:
 * https://docsdev.fieldclimate.com/#6187d2ac-b6cd-424c-bb80-bfe7fc5428f8
 */
$request = "/data/{$stationID}/{$dataGroup}/from/{$from}/to/{$to}";

/**
 * HMAC authentication needs
 */
$timestamp = gmdate('D, d M Y H:i:s T');
$content_to_sign = $method . $request . $timestamp . $public_key;
$signature = hash_hmac("sha256", $content_to_sign, $private_key);
$headers = [
    "Accept: application/json",
    "Authorization: hmac {$public_key}:{$signature}",
    "Date: {$timestamp}"
];

// Prepare and make https request
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $API_URL . $request);
// SSL important
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
if ($method == 'POST') {
    curl_setopt($ch, CURLOPT_POST, 1);
}
$output = curl_exec($ch);
curl_close($ch);

// Parse response as json and work on it ..
header("Content-type", "application/json");
echo $output . PHP_EOL;
