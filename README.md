# METOS API Examples #

A collection of examples how to user Field Climate API.
### PHP/Rain ###

Shows all user stations and informations about their rain data for 24 hours, 48 hours and for 7 days on Map.
User can disable and enable specific stations from Station List and choose between different groups of stations, based on daily precipitation.

### PHP/Excel ###
User can see all collected data from every station seperated in Excel file.

### PHP ###
<code>connection_api_get_method.php</code> - How to connect API using HMAC and GET Method
<code>connection_api_post_method.php</code> - How to connect API using HMAC and POST Method
<code>connection_api_post_forecast_data.php</code> - How to connect API using HMAC, POST Method and return Forecast

### Javascript ###
<code>ConnectionApi.js</code> - How to connect API with Javascript

### Python ###
<code>ConnectionApi.py</code> - How to connect API with Python

### Dart ###
<code>index.dart</code> - How to connect API with Dart
