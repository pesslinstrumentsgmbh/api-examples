// It is necessary the lib CryptoJS
//https://cdnjs.com/libraries/crypto-js

const API = {
    async getFetch(params) {
      const method = params.method
      const body = params.body
      const public_key = "YOUR-PUBLIC-KEY";
      const private_key = "YOUR-PRIVATE-KEY";
      const baseurl = "https://api.fieldclimate.com/v2";
      const timestamp = new Date().toUTCString(); 
      const content_to_sign = params.method + params.request + timestamp + public_key;
      const signature = CryptoJS.HmacSHA256(content_to_sign, private_key);
      const hmac_str = 'hmac ' + public_key + ':' + signature;
      const url = baseurl + params.request;
      const parameters = {
          headers:{
              "Accept":"application/json",
              "Authorization": hmac_str,
              "Request-Date": timestamp,
          },
          method,
          body
      };
  
      return await fetch(url, parameters)
          .then(data => { return data.json() })
          .then((resp) => {
              return resp;
          })
          .catch(error => console.log(error))
    },
      getStations() {
          let params = {
              "method" : "GET",
              "request": "/user/"
          }
          API.getFetch(params)
          .then((result) => {
              result.map((userData, key) => {
                  console.log(userData);
              })
          })
      },
  }
  